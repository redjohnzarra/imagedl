<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/jquery-ui.min.css',
        'css/jquery.dataTables.min.css',
        'css/jquery.fancybox.css',
        'css/site.css'
    ];
    public $js = [
        'js/jquery-2.1.4.min.js',
        'js/jquery-ui.min.js',
        'js/jquery.dataTables.min.js',
        'js/jquery.fancybox.pack.js',
        'js/imagedl.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
