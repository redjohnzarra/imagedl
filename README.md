### Test running INSTRUCTIONS ###

Upon cloning, go to the directory via terminal.
Then run

```
composer install
```

After the update is finished, be sure to change the db connection details, (e.g db name, user, password) on "config/db.php".

After that, you may run

```
java -jar selenium-server-standalone-2.47.1.jar -Dwebdriver.chrome.driver="C:/xampp/htdocs/imageDl/chromedriver.exe"
```

change "C:/xampp/htdocs/imageDl/chromedriver.exe" to the proper file location where you've cloned the project

and on a separate terminal window, go to the project directory, and run

```
php codecept.phar build
```

then,

```
php codecept.phar run
```
considering your php is already added to Environment variables.