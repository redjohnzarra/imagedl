<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Image Downloader';
?>
<script>var BASE_URL = <?php echo'"http://'.$_SERVER['HTTP_HOST'].Yii::$app->homeUrl.'"'; ?></script>
<div class="site-content">
    <?php
        if(Yii::$app->session->hasFlash('empty')){
            echo '<div class="alert alert-danger fade in" style="width:75%;position:absolute">
                    <button class="close" data-dismiss="alert">×</button>
                    '.Yii::$app->session->getFlash('empty').'
                </div>';
        }
    ?>
    <div class="row row-main">
        <div class="col-lg-7 centered">
            <?php $form = ActiveForm::begin();
                echo $form->field($model, 'url', [
                        "inputOptions" => [
                            "placeholder" => 'Ex: https://www.google.com',
                            "style" => "width: 100%",
                            "class" => 'form-control'
                        ],
                        "labelOptions" => [
                            "label" => "Enter URL here: "
                        ]
                    ]);
                echo Html::submitButton('Go', ['class'=>'btn btn-success', 'style'=>'float:right']);
            ?>
        </div>
    </div>
</div>
