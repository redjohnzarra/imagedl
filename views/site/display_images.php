<?php
    $this->title = empty($images) ? 'No Images Found' : 'Downloading. . .';
?>
<div class="images-con centered">
<script>var BASE_URL = <?php echo'"http://'.$_SERVER['HTTP_HOST'].Yii::$app->homeUrl.'"'; ?></script>
<?php
    if(!empty($images)){
?>
    <table class="tb-dl">
        <thead>
            <th>Image Name</th>
            <th>Progress</th>
        </thead>
    <tbody>
    <?php
    foreach($images as $image){
        $imageDetails = pathinfo($image);
        $imageName = $imageDetails['filename'].".".$imageDetails['extension'];
        $imagePath = $folderPath."/".$imageName;
        echo '
                <tr>
                    <td><div>'.$imageName.'</div></td>
                    <td>
                    <div style="text-align:center"><div id="'.$imageDetails['filename'].'" data-url="'.$image.'" data-folder="'.$folderPath.'" class="progress default"><div></div></div>
                    </div>
                    </td>
                </tr>
            ';

    }
    ?>
    </tbody>
    </table>
<?php
}
    /*try {
        if(!file_exists($imagePath)){
            $cont = file_get_contents($image);
            $fp = fopen($imagePath, "w");
            fwrite($fp, $cont);
            fclose($fp);
        }
    } catch (Exception $e) {

    }*/
?>
</div>
