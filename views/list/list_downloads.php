<?php
    $this->title = "Downloaded List";
?>
<script>var BASE_URL = <?php echo'"http://'.$_SERVER['HTTP_HOST'].Yii::$app->homeUrl.'"'; ?></script>
<div id="tabs-container" class="tabs-all">
    <ul class="dl-tabs-ul">
        <li class="dl-li"><a class="tab-label-dl" href="#tab-list">List</a></li>
    </ul>
    <div id="tab-list" class="tab-cont">
        <h4>URL List</h4>
        <table class="tb-dlist display" cellspacing="0">
           <?php
              if(empty($rows)){
                 echo '<thead><tr><th></th></tr></thead><tbody><tr><td id="tdNoDownloads" style="text-align:center">Sorry, no downloads yet.</td></tr></tbody>';
              }else{
                 echo '<thead>
                          <tr>
                             <th>URL</th>
                             <th>Number of Images Downloaded</th>
                             <th>Delete</th>
                          </tr>
                       </thead>
                          <tbody>';
                 foreach($rows as $row){
                    $urlArr = json_decode($row['images']);
                    echo '
                             <tr onclick="openUrlDownloads(\''.$row['id'].'\', \''.count($urlArr).'\')">
                                <td>'.$row['url'].'</td>
                                <td>'.count($urlArr).'</td>
                                <td><button class="btn btn-danger" onclick="confirmDelete(\''.$row['id'].'\')">Delete</td>
                             </tr>
                       ';
                 }
                 echo '</tbody>';
              }
           ?>
        </table>
    </div>
    <div id="confirm-dialog"></div>
    <div id="success-dialog"></div>
</div>
