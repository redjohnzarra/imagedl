<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\db\Command;
use app\models\ImageDl;

use app\controllers\SiteController;

/**
 * Class ListController
 * @package app\controllers
 */
class ListController extends Controller
{

    /**
     * @return string
     */
    public function actionDownloads(){
        $scObj = new SiteController("list", Yii::$app);
        $q = new Query;
        $q->select("*")
            ->from($scObj->tbName);
        $rows = $q->all();

        return $this->render('list_downloads', ['rows'=>$rows]);
    }

    /**
     * @return string
     */
    public function actionGetSingle(){
        if(array_key_exists("url_id", $_POST)){
            $scObj = new SiteController("list", Yii::$app);
            $urlId = $_POST['url_id'];
            $q = new Query;
            $q->select("*")
                ->from($scObj->tbName)
                ->where("id = '".$urlId."'");
            $rows = $q->all();
        }else{
            $urlId = '';
            $rows = array();
        }

        $this->layout = false;
        return $this->render('list_single', array("urlId" => $urlId, "rows" => $rows));
    }

    /**
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionDeleteRecord(){
        $scObj = new SiteController("list", Yii::$app);
        if(array_key_exists("url_id", $_POST)){
            $urlId = $_POST['url_id'];
            $conn = Yii::$app->db;
            $rowExec = $conn->createCommand("DELETE FROM $scObj->tbName WHERE id = '$urlId'")->execute();
            $folderPath = 'downloads/'.$urlId;
            $this->removeDir($folderPath);
        }

        if(isset($rowExec)){
            return "Success";
        }else{
            return "Fail";
        }
    }

    /**
     * @param $target
     */
    public function removeDir($target) {
        if(is_dir($target)){
            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

            foreach( $files as $file )
            {
                $this->removeDir( $file );
            }

            if(file_exists($target)) rmdir( $target );
        } elseif(is_file($target)) {
            unlink( $target );
        }
    }

}

?>
