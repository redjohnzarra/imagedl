<?php

namespace app\controllers;

use yii\db\Schema;
use yii\db\Migration;

class MigrateController extends Migration
{
    public function up()
    {
        $this->createTable('image_dl', [
            'id' => $this->primaryKey(),
            'url' => $this->string()->notNull(),
            'images' => $this->text(),
        ]);
    }

    public function down()
    {
        /*echo "m151214_032403_image_dl cannot be reverted.\n";

        return false;*/
        $this->dropTable('image_dl');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
