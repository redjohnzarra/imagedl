<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\db\Command;
use app\models\UrlForm;
use app\models\ImageDl;

use app\controllers\MigrateController;
use yii\web\Session;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @var string
     */
    public $tbName = 'image_dl';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @return string
     */
    public function getTbName(){
        return $this->tbName;
    }

    /**
     * @return string
     * @throws \yii\base\ExitException
     * @throws \yii\db\Exception
     */
    public function actionIndex(){
        $model = new UrlForm;
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $table = Yii::$app->db->schema->getTableSchema($this->tbName);
            if(empty($table)){
                $migrate = new MigrateController;
                $migrate->up();
            }

            if(array_key_exists('url', $_POST['UrlForm'])){
                $url = $_POST['UrlForm']['url'];
                $c = curl_init();
                curl_setopt_array($c, array(
                    CURLOPT_URL => $url,

                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HEADER => FALSE,
                    CURLOPT_SSL_VERIFYHOST =>FALSE,
                    CURLOPT_SSL_VERIFYPEER => FALSE,

                    CURLOPT_FOLLOWLOCATION => TRUE,
                    CURLOPT_MAXREDIRS => 10,

                    CURLOPT_REFERER => $url,
                ));

                $response = curl_exec($c);
                curl_close($c);

                $document = new \DOMDocument();
                if($response){
                    libxml_use_internal_errors(true);
                    $document->loadHTML($response);
                    libxml_clear_errors();
                }

                $urlArr = parse_url($url);
                $base = $urlArr['scheme'].'://'.$urlArr['host'];
                $images = array();

                foreach($document->getElementsByTagName('img') as $img){
                    $imgUrl = $img->getAttribute('src');
                    if($this->stringStartsWith($imgUrl, "//")){
                        $imageStr = str_replace("//", "", $imgUrl);
                        $imgUrl = strstr($imageStr, '/');
                    }

                    $imageSrc = $this->makeAbsolute($imgUrl, $base);
                    if(!$imageSrc || strpos($imageSrc, "?") !== false)
                        continue;

                    $imageDetails = pathinfo($imageSrc);

                    if(array_key_exists("extension", $imageDetails)){
                        if(!in_array($imageSrc, $images)) $images[] = $imageSrc;
                    }
                }

                $query = new Query;
                $query->select('id, url')
                    ->from($this->tbName)
                    ->where("url = '$url'");

                $rows = $query->all();
                $conn = Yii::$app->db;

                if(empty($images)){
                    Yii::$app->session->setFlash('empty', 'No images found on the url given');
                    return $this->render('index', ['model'=>$model]);
                }else{
                    if(empty($rows)){
                        $model = new ImageDl;
                        $model->url = $url;
                        $model->images = json_encode($images);
                        if($model->validate()){
                            $model->save();
                            $recordId = $model->id;
                        }else{
                            CVarDumper::dump($model->getErrors(),5678,true);
                            Yii::$app->end();
                        }
                    }else{
                        $recordId = $rows[0]['id'];
                        $conn->createCommand("UPDATE $this->tbName SET images = '".json_encode($images)."' WHERE url = '$url'")
                            ->execute();
                    }

                    if(!file_exists('downloads')){
                        mkdir('downloads', 0777);
                    }

                    $folderPath = 'downloads/'.$recordId;
                    if(!file_exists($folderPath)){
                        mkdir($folderPath, 0777);
                    }

                    return $this->render('display_images', array("folderPath"=>$folderPath, "images"=>$images));
                }
            }
        }else{
            return $this->render('index', ['model'=>$model]);
        }

    }

    /**
     * @param $string
     * @param $prefix
     * @return bool
     */
    public function stringStartsWith($string, $prefix){
        return 0 === strpos($string, $prefix) ? true: false;
    }

    /**
     * @param $url
     * @param $base
     * @return string
     */
    public function makeAbsolute($url, $base){
        // Return base if no url
        if( ! $url) return $base;

        // Return if already absolute URL
        if(parse_url($url, PHP_URL_SCHEME) != '') return $url;

        // Urls only containing query or anchor
        if($url[0] == '#' || $url[0] == '?') return $base.$url;

        // Parse base URL and convert to local variables: $scheme, $host, $path
        extract(parse_url($base));

        // If no path, use /
        if( ! isset($path)) $path = '/';

        // Remove non-directory element from path
        $path = preg_replace('#/[^/]*$#', '', $path);

        // Destroy path if relative url points to root
        if($url[0] == '/') $path = '';

        // Dirty absolute URL
        $abs = "$host$path/$url";

        // Replace '//' or '/./' or '/foo/../' with '/'
        $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
        for($n = 1; $n > 0; $abs = preg_replace($re, '/', $abs, -1, $n)) {}

        // Absolute URL is ready!
        return $scheme.'://'.$abs;
    }

    /**
     * @return string
     */
    public function actionDownload(){
        if(array_key_exists("image_url", $_POST)){
            $imageUrl = $_POST['image_url'];
            $imageDetails = pathinfo($imageUrl);
            $imageName = $imageDetails['filename'].".".$imageDetails['extension'];
            $imagePath = $_POST['folder_path']."/".$imageName;
            $_p = fopen("progress.txt", "w");
            fwrite($_p, 0);
            fclose($_p);
            if(!file_exists($imagePath)){
                // $this->downloadImage($imageUrl, $imagePath);
                // file_put_contents("progress.txt", "");
                $ctx = stream_context_create();
                stream_context_set_params($ctx, array("notification" => array($this, "streamNotifCallback")));
                copy($imageUrl, $imagePath, $ctx);
            }

            $_p = fopen("progress.txt", "w");
            fwrite($_p, 100);
            fclose($_p);

            return "Success";
        }
    }

    /**
     * @param $notification_code
     * @param $severity
     * @param $message
     * @param $message_code
     * @param $bytes_transferred
     * @param $bytes_max
     */
    function streamNotifCallback($notification_code, $severity, $message, $message_code, $bytes_transferred, $bytes_max) {

        switch($notification_code) {
            case STREAM_NOTIFY_RESOLVE:
            case STREAM_NOTIFY_AUTH_REQUIRED:
            case STREAM_NOTIFY_COMPLETED:
                break;
            case STREAM_NOTIFY_FAILURE:
                break;
            case STREAM_NOTIFY_AUTH_RESULT:
                // var_dump($notification_code, $severity, $message, $message_code, $bytes_transferred, $bytes_max);
                /* Ignore */
                break;

            case STREAM_NOTIFY_REDIRECTED:
                // echo "Being redirected to: ", $message;
                break;

            case STREAM_NOTIFY_CONNECT:
                // echo "Connected...";
                break;

            case STREAM_NOTIFY_FILE_SIZE_IS:
                // echo "Got the filesize: ", $bytes_max;
                break;

            case STREAM_NOTIFY_MIME_TYPE_IS:
                // echo "Found the mime-type: ", $message;
                break;

            case STREAM_NOTIFY_PROGRESS:
                $percentDownloaded = $bytes_max > 0 ? ($bytes_transferred/$bytes_max) * 100 : 0;
                // echo "Made some progress, downloaded ", $bytes_transferred, " so far";
                // echo "About ", $percentDownloaded ,"%";
                $_p = fopen("progress.txt", "w");
                fwrite($_p, number_format($percentDownloaded));
                fclose($_p);
                break;
        }
        echo "\n";
    }

    /**
     * @param $imageUrl
     * @param $imagePath
     * @return string
     */
    public function downloadImage($imageUrl, $imagePath){
        file_put_contents("progress.txt", "0");
        $targetFile = fopen($imagePath, 'w' );
        $ch = curl_init($imageUrl);
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_SSL_VERIFYHOST =>FALSE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_NOPROGRESS => FALSE,
            CURLOPT_PROGRESSFUNCTION => array($this, 'progressCallback'),
            CURLOPT_FILE => $targetFile,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_MAXREDIRS => 10,

            CURLOPT_REFERER => $imageUrl,
        ));
        $success = curl_exec($ch);
        if(!$success){
            return curl_error($ch);
        }
        curl_close($ch);
        fclose($targetFile);
    }

    /** callback function for curl */
    function progressCallback($resource, $downloadSize, $downloadedSize, $uploadSize, $uploadedSize){
        static $previousProgress = 0;

        if ( $downloadSize == 0 )
            $progress = 0;
        else
            $progress = round( $downloadedSize * 100 / $downloadSize );

        if ( $progress > $previousProgress){
            $previousProgress = $progress;
            $fp = fopen( 'progress.txt', 'a' );
            fputs($fp, "$progress\n");
            fclose($fp);
        }
    }
}
