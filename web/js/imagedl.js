// var BASE_URL = "http://localhost/imageDl/web/index.php";
var activeAjaxCount = 0;
var progTimer;
var imgsArr = [];
var i =0;

$(document).ready(function(){
    if($("div.images-con").length){
        if($("table.tb-dl").length){
            $("div.progress").each(function(y, elem){
                imgsArr.push($(elem));
            }).promise().done( function(){
                if(imgsArr.length > 0){
                    downloadImage(imgsArr[0], imgsArr);
                }
            });
        }
    }

    if($(".tabs-all").length){
        $(".tabs-all").tabs();
    }

    if($("table.tb-dlist").length){
        $("table.tb-dlist").DataTable();
    }
});

function progress(percent, $element) {
    if(percent != 0){
        var progressBarWidth = percent * $element.width() / 100;
        $element.find('div').animate({ width: progressBarWidth }, 500).html(percent + "% ");
    }
}

function downloadImage($elem, imgsArr){
    var imageUrl = $elem.attr("data-url");
    var folderPath = $elem.attr("data-folder");
    activeAjaxCount = activeAjaxCount + 2;
    progTimer = setInterval(function(){
        getProgress($elem);
    }, 1000);
    /*var progTimer = setInterval(function(){
        console.log("Hello");
        $.get("progress.txt", function(data) {
            var percentage = parseInt(data);
            console.log(percentage);
            progress(percentage, $elem);
            if(percentage == 100){
                if(activeAjaxCount > 0) activeAjaxCount--;
                clearInterval(progTimer);
            };
       });
    }, 1000);*/
    $.ajax({
        url: BASE_URL + "?r=site/download",
        type: "POST",
        data: {
            _csrf: yii.getCsrfToken(),
            image_url: imageUrl,
            folder_path: folderPath
        },
        success: function(response){
            i++;
            if(activeAjaxCount > 0){
                activeAjaxCount--;
            }
            if(response.indexOf("Success") > -1){

            }else{
                $elem.html("Image Not Found");
            }

            if(i < imgsArr.length){
                var checkAjaxTimer = setInterval(function(){
                    if(activeAjaxCount == 0){
                        clearInterval(checkAjaxTimer);
                        downloadImage(imgsArr[i], imgsArr);
                    }
                }, 1000);
            }else{
                setTimeout(function(){
                    $("table.tb-dl").hide();
                    $("div.images-con").html('<div class="alert alert-success fade in" style="width:100%"><button class="close" data-dismiss="alert">×</button>Download Complete. Reloading in 3 seconds. . .</div>');
                    setTimeout(function(){
                        window.location.replace(BASE_URL);
                    }, 3000);
                }, 2000);

            }
        }
    });
}

function getProgress($elem){
    $.get("progress.txt", function(data){
        var percentage = parseInt(data);
        if(percentage == 100){
            if(activeAjaxCount > 0){
                activeAjaxCount--;
            }
            clearInterval(progTimer);
        }

        progress(percentage, $elem);
    });

}

function openUrlDownloads(urlId, countImgs){
    $.ajax({
        url: BASE_URL + "?r=list/get-single",
        type: "POST",
        data: {
            _csrf: yii.getCsrfToken(),
            url_id: urlId,
        },
        success: function(response){
            if(!$("li.dl-ls").length){
                $("ul.dl-tabs-ul").append('<li class="dl-ls"><a class="tab-label-ds" href="#tab-single">Images ('+countImgs+')</a><span class="ui-icon ui-icon-circle-close ui-closable-tab"></span></li>');
                $("div.tabs-all").append("<div id='tab-single'></div>");
            }
            $("div#tab-single").html(response);
            $(".tabs-all").tabs("refresh");
            $(".tabs-all").tabs("option", "active", -1);

            $(".ui-closable-tab").on("click", function() {
                var tabContainerDiv=$(this).closest(".ui-tabs").attr("id");
                var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
                $( "#" + panelId ).remove();
                $("#"+tabContainerDiv).tabs("refresh");
                var tabCount=$("#"+tabContainerDiv).find(".ui-closable-tab").length;
                if (tabCount<0) {
                    $("#"+tabContainerDiv).hide();
                }
            });

            $(".fb-img").fancybox();
        }
    });
}

function confirmDelete(urlId, e){
    if (!e)
      e = window.event;

    //IE9 & Other Browsers
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    //IE8 and Lower
    else {
      e.cancelBubble = true;
    }
   /* var conf = confirm("Are you sure you want to proceed?");
    if(conf){
        $.ajax({
            url: BASE_URL + "?r=list/delete-record",
            type: "POST",
            data: {
                _csrf: yii.getCsrfToken(),
                url_id: urlId,
            },
            success: function(response){
                alert(response);
                window.location.reload();
            }
        });
    }*/
    $("#confirm-dialog").html("Are you sure you want to proceed?");
    $("#confirm-dialog").dialog({
        resizable: false,
        modal: true,
        open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
        height: 150,
        width: 350,
        buttons: {
            "Yes": {
                text: "Yes",
                id: "yesBtn",
                class: "btn btn-success",
                click: function () {
                    $(this).dialog('close');
                    deleteRecord(urlId);
                }
            },
            "No": {
                text: "No",
                id: "noBtn",
                class: "btn btn-danger",
                click: function () {
                    $(this).dialog('close');
                }
            }
        }
    });
}

function deleteRecord(urlId){
    $.ajax({
        url: BASE_URL + "?r=list/delete-record",
        type: "POST",
        data: {
            _csrf: yii.getCsrfToken(),
            url_id: urlId,
        },
        success: function(response){
            $("#success-dialog").html("<div class='alert alert-success fade in' style='text-align: center;vertical-align: middle;line-height: 30px'><strong>Success!</strong></div>");
            $("#success-dialog").dialog({
                height: 140,
                dialogClass: "alert-success",
                modal: true,
                open: function(event, ui){
                    $(this).parent().find('div.ui-dialog-titlebar').addClass('alert alert-success');
                 setTimeout(function(){
                    $('#success-dialog').dialog('close');
                    window.location.reload();
                 },1500);
                }
            });
            $(".ui-dialog-titlebar").hide();
        }
    });
}
