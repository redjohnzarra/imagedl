<?php

/*** A URL may be added to this array for testing purposes. ***/

$urls = array("https://www.vagrantup.com", "http://www.300mbfilms.org", "https://www.google.com");

foreach($urls as $url){
    startTest($url, $scenario);
}

startTest2($urls, $scenario);

function startTest($url, $scenario){
    $st = 1;
    $I = new AcceptanceTester($scenario);
    enterUrl($url, $I);
    sleep($st);
    downloadingImages($I);
    $I->click('Downloads');
    sleep($st);
    goToDownloads($I, $st);
    $I->click('table.tb-dlist tbody tr');
    sleep($st);
    $I->waitForElement('a.fb-img:nth-child(1)');
    $I->click('a.fb-img:nth-child(1)');
    sleep($st);
    browseSingleUrlDownloads($I);
    deleteUrlDownloads($I, $st);
    goToHome($I);
    sleep($st);
}

function startTest2($urls, $scenario){
    $st = 1;
    $I = new AcceptanceTester($scenario);
    foreach($urls as $url){
        enterUrl($url, $I);
        sleep($st);
        downloadingImages($I);
        $I->waitForElementNotVisible('div.alert.alert-success');
    }
    goToDownloads($I, $st);
    $rowLength = '$("table.tb-dlist tbody tr").length';
    $rowCount = $I->executejs('return ' . $rowLength);
    for($x = 1; $x <= $rowCount; $x++){
        sleep($st);
        $I->waitForElement('table.tb-dlist tbody tr:nth-child('.$x.')');
        $I->click('table.tb-dlist tbody tr:nth-child('.$x.')');
        sleep($st);
        $I->waitForElement('a.fb-img:nth-child(1)');
        $I->click('a.fb-img:nth-child(1)');
        sleep($st);
        browseSingleUrlDownloads($I);
    }

    for($x = 1; $x <= $rowCount; $x++){
        deleteUrlDownloads($I, $st, $x);
    }
    goToHome($I);
    sleep($st);
}

/**
 * @param $I
 */
function goToHome($I)
{
    $I->wantTo('go to Home page');
    $I->waitForElementNotVisible('div.ui-dialog');
    $I->waitForElement('td#tdNoDownloads');
    $I->click('Image Downloader');
}

/**
 * @param $I
 * @param $st
 * @param bool $specific
 */
function deleteUrlDownloads($I, $st, $sp = false)
{
    $I->wantTo('delete URL downloads');
    $I->waitForElementNotVisible('li.dl-ls', 10);
    if($sp === false){
        $I->waitForElement('button.btn-danger');
        $I->click('button.btn-danger');
    }else{
        sleep(2);
        $I->waitForElement('button.btn-danger:nth-child(1)');
        $I->click('button.btn-danger:nth-child(1)');
    }
    $I->waitForElement('div.ui-dialog');
    $I->see('Are you sure you want to proceed?');
    sleep($st);
    $I->click('button#yesBtn');
    $I->waitForElementNotVisible('div.alert.alert-success');
}

/**
 * @param $I
 */
function browseSingleUrlDownloads($I)
{
    $I->wantTo('browse my downloads for this URL');
    $classCount = '$("a.fb-img").length';
    $limit = $I->executejs('return ' . $classCount);
    if ($limit > 1) {
        $I->waitForElement('a[title=Next]');
        // $limit = rand(3,10);
        for ($i = 1; $i < $limit; $i++) {
            $I->waitForElement('a[title=Next]');
            $I->click('a[title=Next]');
        }
    }
    $I->waitForElement('a[title=Close]');
    $I->click('a[title=Close]');
    $I->waitForElementNotVisible('div.fancybox-overlay', 10);
    $I->click('span.ui-closable-tab');
}

/**
 * @param $I
 */
function goToDownloads($I, $st)
{
    $I->wantTo('go to Downloads page');
    $I->click('Downloads');
    sleep($st);
    $I->see('URL List');
}

/**
 * @param $I
 */
function downloadingImages($I)
{
    $I->seeInTitle('Downloading');
    $I->waitForElement('div.alert-success', 300);
    $I->see('Download Complete');
}

/**
 * @param $url
 * @param $scenario
 * @return AcceptanceTester
 */
function enterUrl($url, $I)
{
    $I->wantTo('see Image Downloader word in title');
    try {
        $I->amOnPage('/');
    } catch (Exception $e) {
        $I->amOnPage('/index.php');
    }
    $I->seeInTitle('Image Downloader');
    $I->waitForElement('input#urlform-url');
    $I->fillField('#urlform-url', $url);
    $I->pressKey('#urlform-url', WebDriverKeys::ENTER);
    return $I;
}
