<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "image_dl".
 *
 * @property integer $id
 * @property string $url
 * @property string $images
 */
class ImageDl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image_dl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['images'], 'string'],
            [['url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'images' => 'Images',
        ];
    }
}
